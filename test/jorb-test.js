import $ from 'jquery'
import ko from 'knockout'
import sinon from 'sinon'

window.$ = $
window.ko = ko
window.jQuery = $

require('knockout.validation')

const server = sinon.fakeServer.create({ respondImmediately: true })

function setup() {

}

function teardown() {
  server.restore()
}

function expectAndDone(done, closure) {
  try {
    closure()
    done()
  } catch (e) {
    done.fail(e)
  }
}

function expectOrDone(done, closure) {
  try {
    closure()
  } catch (e) {
    done.fail(e)
  }
}

export { setup, expectAndDone, expectOrDone, server, teardown }
