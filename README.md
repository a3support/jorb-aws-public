# JORB Development README

## Getting Started

1. install node and npm on your local machine
1. in the root directory, run npm install
1. run npm run watch

This launches webpack in a watching mode and will rebuild as changes require it.
