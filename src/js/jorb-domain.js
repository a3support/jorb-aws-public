import * as log from './jorb-log'
import DomainCache from './jorb-cache'
import { Ajax as Service } from './jorb-ajax'

/* eslint-disable no-unused-vars */
let _cache

class DomainObject {
  constructor(resources, configuration = {}) {
    const defaultOptions = {
      liveEdit: [],
      typeMap: {},
      isParentMap: {},
      onlyMapId: [],
      saveAsDeep: [],
      ignoreOnSave: [
        'isLoaded',
        'isLiveEdit',
        'isPopulating',
        'resources',
        'options',
        'savePromise',
        'flash',
        'isSaving',
        'isLoading',
        'isValid',
        'settingsValidator',
        '_forceValidation',
        '_mutateBeforeValid',
      ],
    }
    const mergedOverride = {}
    if (configuration.ignoreOnSave) {
      mergedOverride.ignoreOnSave = [...configuration.ignoreOnSave, ...defaultOptions.ignoreOnSave]
    }

    this.options = Object.assign({}, defaultOptions, configuration, mergedOverride)
    this.resources = resources
    this.id = ko.observable()
    this.isLoaded = ko.observable(false)
    this.isLiveEdit = ko.observable(false)
    // eslint-disable-next-line
    //this.savePromise = new RiskBeam.ext.FuturePromise()
    this.isPopulating = ko.observable(0).extend({ deferred: true })
    this.isSaving = ko.observable(false)
    // eslint-disable-next-line
    //this.flash = new RiskBeam.ext.FlashQueue()
    this.isLoading = ko.observable(false)
    this._forceValidation = ko.observable(false)
    this.currentLoadPromise = undefined
    this.skipValidation = false

    $.each(this.options.liveEdit, (index, propToLiveEdit) => {
      if (this[propToLiveEdit]) {
        const prop = this[propToLiveEdit]
        if (ko.isObservable(prop)) {
          prop.extend({ liveEdit: { scope: this, name: 'save', guard: this.isLiveEdit } })
          prop.extend({ pauseForBinding: this.isPopulating })
        } else {
          log.log(`Skipping non-observable property ${propToLiveEdit} for live editing`)
        }
      } else {
        log.log(`Skipping missing property ${propToLiveEdit} for live editing.`)
      }
    })

    this._mutateBeforeValid = ko.observable(false)

    this.isValid = ko
      .pureComputed(() => {
        this._forceValidation()
        let valid = true
        $.each(this, (name, prop) => {
          if (ko.isObservable(prop) && ko.validation.utils.isValidatable(prop)) {
            if (this._mutateBeforeValid()) {
              prop.valueHasMutated()
            }
            if (!prop.isValid()) {
              valid = false
            }
            if (prop() instanceof DomainObject) {
              if (prop().isLoaded() && !prop().isValid()) {
                valid = false
              }
              if (ko.computedContext.isInitial() && ko.computedContext.registerDependency) {
                ko.computedContext.registerDependency(prop().isValid)
              }
            }
            if (ko.computedContext.isInitial() && ko.computedContext.registerDependency) {
              ko.computedContext.registerDependency(prop)
            }
          }
        })
        return valid
      })
      .extend({ notify: 'always' })
  }

  startPopulating() {
    this.isPopulating(this.isPopulating() + 1)
  }

  donePopulating() {
    this.isPopulating(this.isPopulating() - 1)
  }

  static enableCache() {
    if (!_cache) {
      _cache = new DomainCache()
    }
  }

  static disableCache() {
    _cache = undefined
  }

  static get(id, callback, options) {
    const self = _cache ? _cache.get(this, id) : new this()
    const query = options || {}
    const onSuccess = (response) => {
      try {
        self.populate(response.object)
      } catch (ex) {
        log.warn(ex)
      }
      self.isLoaded(true)
      if (callback && typeof callback === 'function') {
        callback(self)
      }
    }

    if (typeof callback === 'boolean' && !callback) {
      return self
    }

    new Service('get', 'json').makeCall(`/api/${this.resources()}/${id}`, query, onSuccess)
    return self
  }

  static list(query, callback, destination) {
    const values = destination || ko.observableArray([]).extend({ deferred: true })
    const self = this
    const onSuccess = (response) => {
      if (destination) destination.removeAll()
      if (response.success) {
        $.each(response.object, (index, item) => {
          // eslint-disable-next-line new-cap
          const newItem = _cache ? _cache.get(self, item.id) : new self()
          newItem.populate(item)
          values.push(newItem)
        })
        if (callback && typeof callback === 'function') {
          callback(values)
        }
      } else {
        log.error('Error on response')
      }
    }

    let url = this.resources()
    if (query) {
      $.each(query, (k, v) => {
        let hasArrayAdded = false
        if ($.isArray(v)) {
          if (!hasArrayAdded) {
            url += '?'
            hasArrayAdded = true
          } else {
            url += '&'
          }
          const vars = v.join(`&${k}=`)
          url += `${k}=${vars}`
          // eslint-disable-next-line
          delete query[k]
        }
      })
    }

    new Service('get', 'json').makeCall(`/api/${url}`, query, onSuccess)
    return values
  }

  delete(callback) {
    if (this.id && this.id()) {
      const self = this
      const onSuccess = () => {
        if (_cache) _cache.evict(self, this.id())
        this.id(null)
        if (typeof callback === 'function') {
          callback(self)
        }
      }

      return new Service('delete', 'json').makeCall(
        `/api/${this.resources()}/${this.id()}`,
        {},
        onSuccess,
      )
    }
    return null
  }

  loadIfNot(deep) {
    const deferred = $.Deferred()
    if (!this.isLoaded()) {
      this.load(() => {
        deferred.resolve()
      }, deep).fail(() => {
        deferred.reject()
      })
    } else {
      deferred.resolve()
    }
    return deferred
  }

  load(callback, deep) {
    if (this.id && this.id()) {
      const self = this
      if (!this.isLoading()) {
        this.isLoading(true)
        const onSuccess = (response) => {
          this.populate(response.object, deep)
          this.isLoaded(true)
          this.isLoading(false)
          if (typeof callback === 'function') {
            callback(self)
          }
          if (this.options.onLoad && typeof this.options.onLoad === 'function') {
            this.options.onLoad.call(self)
          }
        }

        const isDeep = deep || false

        this.currentLoadPromise = new Service('get', 'json').makeCall(
          `/api/${this.resources()}/${this.id()}+?deep=${isDeep}`,
          {},
          onSuccess,
        )
        return this.currentLoadPromise
      }
      this.currentLoadPromise.done(callback)
      return this.currentLoadPromise
    }
    this.currentLoadPromise.done(callback)
    return this.currentLoadPromise
  }

  getSavePromise() {
    return this.savePromise.nextPromise
  }

  asData(objectToFill) {
    const self = this
    const val = objectToFill || {}
    $.each(self, (name, item) => {
      if (ko.isObservable(item)) {
        if (this.ignoreOnSave.indexOf(name) === -1) {
          if (ko.validation.utils.isValidatable(item)) {
            // only include field that are valid.
            if (item.isValid()) {
              this.mapToData(val, name, item)
            }
          } else {
            this.mapToData(val, name, item)
          }
        }
      }
    })
    return val
  }

  // helper function for mapping data from objects
  mapToData(data, name, item) {
    if (this.options.typeMap[name]) {
      if (item() && $.isArray(item())) {
        // eslint-disable-next-line
        data[name] = []
        $.each(item(), (dataIndex, dataItem) => {
          if (dataItem instanceof DomainObject && dataItem.id()) {
            if (this.options.saveAsDeep.indexOf(name) === -1) {
              data[name].push(dataItem.id())
            } else {
              data[name].push(dataItem.asData())
            }
          } else if (
            dataItem instanceof DomainObject &&
            this.options.onlyMapId.indexOf(name) === -1
          ) {
            data[name].push(dataItem.asData())
          }
        })
      } else if (item() && item() instanceof DomainObject) {
        if (item().id()) {
          if (this.options.saveAsDeep.indexOf(name) === -1) {
            // eslint-disable-next-line
            data[name] = item().id()
          } else {
            // eslint-disable-next-line
            data[name] = item().asData()
          }
        } else if (this.options.onlyMapId.indexOf(name) === -1) {
          // eslint-disable-next-line
          data[name] = item().asData()
        }
      } else {
        // eslint-disable-next-line
        data[name] = item()
      }
    } else if (item() && item() instanceof Date) {
      // eslint-disable-next-line
      data[name] = moment(item())
        .utc()
        .format('MM/DD/YYYY')
    } else {
      // eslint-disable-next-line
      data[name] = item()
    }
  }

  save(callback) {
    if (!this.isPopulating() && !this.isSaving()) {
      this.isSaving(true)
      const self = this
      // data object for request body
      const data = {
        id: this.id(),
      }
      if (this.isLiveEdit() && this.id()) {
        this.isValid.respondToChange()
        if (this.isValid()) {
          $.each(this.allLiveEditProperties(), (name, item) => {
            if (ko.validation.utils.isValidatable(item) && this.ignoreOnSave.indexOf(name) === -1) {
              if (item.isModified()) {
                // only include field that are valid.
                ko.validation.validateObservable(item)
                if (item.isValid()) {
                  if (item() === null || item() === undefined) {
                    data[name] = null
                  } else if (
                    item() instanceof DomainObject &&
                    (item().skipValidation || item().isValid())
                  ) {
                    this.mapToData(data, name, item)
                  } else if (!(item() instanceof DomainObject)) {
                    this.mapToData(data, name, item)
                  }
                }
              }
            } else if (item() instanceof DomainObject && item().isValid()) {
              this.mapToData(data, name, item)
            }
          })
        }
      } else {
        this.asData(data)
      }

      // only submit if more than the id has made it through.
      if (Object.keys(data).length > 1) {
        const onSuccess = (response) => {
          if (response.success) {
            this.populate(response.object)
            $.each(data, (name) => {
              if (ko.validation.utils.isValidatable(this[name])) {
                this[name].isModified(false)
              }
              if (typeof this[name].flash !== 'undefined') {
                this[name].flash.pushMessage('success', 'Saved')
              }
            })
            this.flash.pushMessage('success', 'Saved')
            if (callback && typeof callback === 'function') {
              callback(self)
            }
          } else if (response.error) {
            // eslint-disable-next-line
            noty({ text: 'There was an error saving.', type: 'error' })
          } else {
            this.applyFieldErrors(self, response.object)
          }
        }

        if (this.id()) {
          return this.savePromise
            .consume(new Service('put', 'json')
              .sendJson(true)
              .makeCall(`/api/${this.resources()}/${this.id()}`, data, onSuccess))
            .always(() => {
              this.isSaving(false)
            })
        }
        return this.savePromise
          .consume(new Service('post', 'json')
            .sendJson(true)
            .makeCall(`/api/${this.resources()}`, data, onSuccess))
          .always(() => {
            this.isSaving(false)
          })
      }
      this.isSaving(false)
      return true
    }
    this.isSaving(false)
    return true
  }

  // eslint-disable-next-line
  setMissingProperty(name, value) {}

  populate(data, wasDeep) {
    this.startPopulating()
    const mapParentSafely = (name, obj) => {
      if (this.options.isParentMap[name]) {
        const parentProp = this.options.isParentMap[name]
        if (typeof obj[parentProp] === 'function') {
          obj[parentProp](this)
        } else {
          // eslint-disable-next-line
          obj[parentProp] = this
        }
        if (ko.validation.utils.isValidatable(obj[parentProp])) {
          obj[parentProp].isModified(false)
        }
      }
    }
    const getObjectToPopulate = (name, id) => {
      const prop = this[name]
      const typeName = this.options.typeMap[name]
      if (prop && ko.isObservable(prop) && prop()) {
        if ($.isArray(prop())) {
          let found
          $.each(prop(), (index, item) => {
            if (item.id() === id) {
              found = item
            }
          })
          if (found) {
            return found
          }
          // eslint-disable-next-line new-cap
          return _cache ? _cache.get(typeName, id) : new typeName()
        } else if (prop() instanceof DomainObject) {
          return _cache ? _cache.get(typeName, id) : prop()
        }
        return prop()
      }
      // eslint-disable-next-line new-cap
      return _cache ? _cache.get(typeName, id) : new typeName()
    }

    $.each(data, (name, value) => {
      if (value && this.options.typeMap[name]) {
        if ($.isArray(value)) {
          const newArr = []
          $.each(value, (index, vItem) => {
            if (vItem) {
              const newVItem = getObjectToPopulate(name, vItem.id)
              newVItem.populate(vItem)
              if (!newVItem.isLoaded()) {
                newVItem.isLoaded(wasDeep)
              }
              mapParentSafely(name, newVItem)
              newArr.push(newVItem)
              // eslint-disable-next-line
              value = newArr
            }
          })
        } else {
          const newValue = getObjectToPopulate(name, value.id)
          newValue.populate(value)
          if (!newValue.isLoaded()) {
            newValue.isLoaded(wasDeep)
          }
          mapParentSafely(name, newValue)
          // eslint-disable-next-line
          value = newValue
        }
      }
      if (this[name]) {
        if (typeof this[name] === 'function') {
          this[name](value)
          // eslint-disable-next-line
          if (value instanceof DomainObject) {
            this[name].extend({ objectEqualityComparer: true })
          }
          if (ko.validation.utils.isValidatable(this[name])) {
            this[name].isModified(false)
          }
        } else {
          this[name] = value
        }
        if (ko.validation.utils.isValidatable(this[name])) {
          this[name].isModified(false)
        }
      } else {
        this.setMissingProperty(name, value)
      }
    })
    ko.tasks.schedule(() => {
      $.each(data, (name) => {
        if (this[name]) {
          if (ko.validation.utils.isValidatable(this[name])) {
            this[name].isModified(false)
          }
        }
      })
      this.donePopulating()
    })
  }
}
// eslint-disable-next-line
//$.extend(domainObject.prototype, RiskBeam.ext.LiveEdit)

export default DomainObject
export { DomainObject }
