ko.bindingHandlers.withFix = {
  init(element, valueAccessor, allBindings, viewModel, bindingContext) {
    let savedNodes
    ko.computed(
      () => {
        const rawValue = valueAccessor()
        const dataValue = ko.utils.unwrapObservable(rawValue)
        const shouldDisplay = dataValue != null // equivalent to isNot ? !dataValue : !!dataValue
        const isFirstRender = !savedNodes
        // Save a copy of the inner nodes on the initial update, but only if we have dependencies.
        if (isFirstRender && ko.computedContext.getDependenciesCount()) {
          savedNodes = ko.utils.cloneNodes(
            ko.virtualElements.childNodes(element),
            true /* shouldCleanNodes */,
          )
        }

        if (shouldDisplay) {
          if (!isFirstRender) {
            ko.virtualElements.setDomNodeChildren(element, ko.utils.cloneNodes(savedNodes))
          }
          // 3.4.2 ko.applyBindingsToDescendants(bindingContext.createStaticChildContext(rawValue), element)
          ko.applyBindingsToDescendants(bindingContext.createChildContext(rawValue), element)
        } else {
          ko.virtualElements.emptyNode(element)
        }
      },
      null,
      { disposeWhenNodeIsRemoved: element },
    )
    return { controlsDescendantBindings: true }
  },
}
ko.expressionRewriting.bindingRewriteValidators.withFix = false // Can't rewrite control flow bindings
ko.virtualElements.allowedBindings.withFix = true
