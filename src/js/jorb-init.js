import * as log from './jorb-log'
import * as router from './jorb-router'

export default (config = {}) => {
  log.info('initializing jorb...')

  if (typeof config.defaultRoute !== 'undefined') {
    log.trace('using defaultRoute provided by config')
    // jorb.defaultRoute = config.defaultRoute
  }

  // config router?
  router.init(typeof config.router !== 'undefined' ? config.router : {})
}
