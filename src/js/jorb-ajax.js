import $ from 'jquery'
import * as log from './jorb-log'

const Ajax = function Ajax(type, dataType = 'json') {
  this.type = type.toUpperCase()
  this._sendJson = false

  this.sendJson = (value) => {
    this._sendJson = value
    return this
  }

  this.makeCall = (url, data, onAjaxSuccess, onAjaxError) => {
    const onAjaxSuccessWrapper = (resp) => {
      if (typeof onAjaxSuccess === 'function') {
        onAjaxSuccess(resp)
      } else if (resp.message !== undefined) {
        // noty({text: resp.message, type: "information"})
      }
    }

    const ajaxOptions = {
      type: type.toUpperCase(),
      url,
      data,
      dataType,
    }
    if (this._sendJson) {
      ajaxOptions.data = JSON.stringify(ajaxOptions.data)
      ajaxOptions.contentType = 'application/json'
    }
    return $.ajax(ajaxOptions)
      .done(onAjaxSuccessWrapper)
      .fail((jHXR) => {
        if (jHXR.status === 401) {
          // eslint-disable-next-line
          // const template = '<div style="padding: 15px"><div style="font-size: 14pt">Your session has expired.</div><div style="font-weight: normal display: inline-block margin-top: 12pt font-size: 12pt">Refresh the page to continue.</div></div>'
          // noty({
          // type:'error',
          // text: template,
          // timeout: false,
          // closeWith: [],
          // modal: true,
          // killer: false,
          // layout: 'center'
          // })
          return
        }

        if (typeof onAjaxError === 'function') {
          onAjaxError()
        } else {
          log.warn('error calling api')
          // noty({text: "Oops! Something went wrong.", type: "error"})
        }
      })
  }
}
export default Ajax
export { Ajax }
