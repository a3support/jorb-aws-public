let lastTs
const $ts = () => {
  const now = new Date()
  let ts = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getMilliseconds()}`

  if (lastTs) {
    ts += ` + ${now.getTime() - lastTs.getTime()}ms`
  }
  lastTs = now

  return ts
}

export default $ts
