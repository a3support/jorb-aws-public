import _ts from './jorb-ts'

// default logging off if not running locally
let loggingEnabled = window.location.hostname === 'localhost'
const logLevel = 0 // 0=trace, 1=debug, 2=info, 3=warn
/* eslint-disable no-console */
const loggers = [
  console.log,
  typeof console.debug === 'undefined' ? console.log : console.debug,
  typeof console.info === 'undefined' ? console.log : console.info,
  typeof console.warn === 'undefined' ? console.log : console.warn,
]
/* eslint-enable no-console */

const log = (msg, level) => {
  let lvl = level
  let message = msg
  if (typeof lvl === 'undefined' || lvl < 0 || lvl > 3) {
    lvl = 0 // default to TRACE
  }
  if (loggingEnabled) {
    if (Array.isArray(message)) message = message.join(' ')
    loggers[lvl].call(null, `jorb [${_ts()}] ${message}`)
  }
}

const _log = (level, msg) => {
  // if no msg, caller wants to know if log level is enabled
  if (typeof msg === 'undefined') return level >= logLevel
  if (level >= logLevel) log(msg, level)
  return this
}

const trace = msg => _log(0, msg)
const debug = msg => _log(1, msg)
const info = msg => _log(2, msg)
const warn = msg => _log(3, msg)

const enableLogging = () => {
  loggingEnabled = true
}

export { logLevel, loggers, log, trace, debug, info, warn, enableLogging }
