import 'font-awesome-webpack'
import init from './src/js/jorb-init'
import * as logging from './src/js/jorb-log'
import * as router from './src/js/jorb-router'
import 'expose-loader?$!expose-loader?jQuery!expose-loader?window.jQuery!jquery'
import 'bootstrap/dist/js/bootstrap'
import 'expose-loader?ko!knockout'
import 'knockout.validation'
import 'knockout-mapping'

import 'noty/js/noty/jquery.noty'
import 'noty/js/noty/layouts/bottom'
import 'noty/js/noty/layouts/bottomCenter'
import 'noty/js/noty/layouts/bottomLeft'
import 'noty/js/noty/layouts/bottomRight'
import 'noty/js/noty/layouts/center'
import 'noty/js/noty/layouts/centerLeft'
import 'noty/js/noty/layouts/centerRight'
import 'noty/js/noty/layouts/inline'
import 'noty/js/noty/layouts/top'
import 'noty/js/noty/layouts/topCenter'
import 'noty/js/noty/layouts/topLeft'
import 'noty/js/noty/layouts/topRight'
import 'noty/js/noty/themes/relax'
import './src/js/ko/withFix'

/*
 * require all src less files so they watched, and ultimately, are copied into dist as LESS files
 * (not compiled into CSS and loaded into bundle)
 */
require.context('./src/less', true)

/*
 * todo: refactor to move modules into separate js files.
 * todo: @davebrown - let's make sure we are in sync on how to organize DomainObject, router, etc.
 */
$.noty.defaults = {
  layout: 'topCenter',
  theme: 'relax', // or 'relax'
  type: 'information',
  text: '', // can be html or string
  dismissQueue: true, // If you want to use queue feature set this true
  template:
    '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
  animation: {
    open: { height: 'toggle' }, // or Animate.css class names like: 'animated bounceInLeft'
    close: { height: 'toggle' }, // or Animate.css class names like: 'animated bounceOutLeft'
    easing: 'swing',
    speed: 250, // opening & closing animation speed
  },
  timeout: 3000, // delay for closing event. Set false for sticky notifications
  force: false, // adds notification to the beginning of queue when set to true
  modal: false,
  maxVisible: 5, // you can set max visible notification for dismissQueue true option,
  killer: true, // for close all notifications before show
  closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
  callback: {
    onShow() {},
    afterShow() {},
    onClose() {},
    afterClose() {},
    onCloseClick() {},
  },
  buttons: false, // an array of buttons
}

const jorb = {
  init,
  router,
  // the default route if no URL fragment is present (as configured by the client app.)
  defaultRoute: null,
  // holds the current "root" view model for application wide access,
  // up to the client application to manage this state, however, jorb
  // utilities will utilize this if present
  viewModel: null,
}

Object.assign(jorb, logging)

export default jorb
export * from './src/js/jorb-domain'
export * from './src/js/jorb-ajax'
export { logging as Log }
