// eslint-disable-next-line no-unused-vars
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WebpackShellPlugin = require('webpack-shell-plugin')
const path = require('path')

const config = {
  devtool: 'source-map',
  entry: ['./jorb.js'],
  resolve: {
    alias: {},
    modules: ['node_modules'],
    extensions: ['.js', '.less'],
  },

  output: {
    filename: 'jorb.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'jorb',
    libraryTarget: 'umd',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/, // Notice the regex here. We're matching on js and jsx files.
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  'env',
                  {
                    targets: {
                      browsers: ['last 2 versions', 'ie >= 9'],
                    },
                  },
                ],
              ],
              plugins: ['transform-class-properties', 'transform-es2015-template-literals'],
            },
          },
        ],
      },
      { test: /(\.woff|\.woff2|\.eot|\.svg|\.ttf)$/, loader: 'ignore-loader' },
      { test: /\.less$/, use: [{ loader: 'ignore-loader' }] },
      /* {
      test: /_jorb-all\.less/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        //resolve-url-loader may be chained before sass-loader if necessary
        use: ['css-loader', 'less-loader']
      })
    } */
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new WebpackShellPlugin({
      // it took me 4 hours to realize that this is important and defaults to true
      dev: false,
      // this script combines multiple less files into one jorb.less in dist
      onBuildEnd: ['./less-combine-jorb.sh'],
    }),
    //  new ExtractTextPlugin('jorb.css'),
    new CopyWebpackPlugin([
      { from: 'src/less/jorb-avenir.less', to: 'less/jorb-avenir.less' },
      { from: 'src/less/avenir-fonts.less', to: 'less/avenir-fonts.less' },
      { from: 'src/less/flex.less', to: 'less/flex.less' },
      { from: 'src/fonts', to: 'fonts' },
      { from: 'vendor/bootstrap-3.3.7/docs', to: 'docs/bootstrap' },
      { from: 'vendor/theme-dashboard/docs', to: 'docs/dashboard-theme' },
      { from: 'package.json', to: 'package.json' },
    ]),
  ],
}

module.exports = config
