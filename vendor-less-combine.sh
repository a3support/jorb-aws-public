#!/usr/bin/env bash
#
# run this to combine the less files from bootstrap and related theme for use in the jorb src dir
#
groovy src/less-combine.groovy ../vendor-less-combine.json