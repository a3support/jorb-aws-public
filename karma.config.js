var webpackConfig = require('./webpack.config.js');
webpackConfig.entry = {};

module.exports = function(config) {
	config.set({
		// base path that will be used to resolve all patterns (eg. files, exclude)
		basePath: '.',
		plugins: [
			'karma-mocha',
			'karma-chai',
			'karma-sinon',
			'karma-phantomjs-launcher',
			'karma-phantomjs-shim',
			'karma-webpack',
		],
		// frameworks to use
		// available frameworks: https://npmjs.org/browse/keyword/karma-adapter
		frameworks: ['mocha', 'chai', 'sinon', 'phantomjs-shim'],
		// list of files / patterns to load in the browser
		files: [
			'test/**/*.js',
		],

		preprocessors: { 
			"src/javascript/jorb.js" : ['webpack'],
			"test/**/*.js": ['webpack']
		},

		// list of files to exclude
		exclude: [
		],

		webpack: webpackConfig,

		webpackMiddleware: {
			stats: 'errors-only'
		},

		// preprocess matching files before serving them to the browser
		// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
		reporters: ['progress'],
		autoWatch: false,
		singleRun: true,
		// enable / disable colors in the output (reporters and logs)
		colors: true,
		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_DEBUG,
		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		browsers: ['PhantomJS'],
		// Concurrency level
		// how many browser should be started simultaneous
		concurrency: Infinity,

		coverageReporter: {
			type: 'html',
			dir: 'build/test-reports/karma'
		}
	});
};

